const {request ,response}=require('express')
const express=require('express')
const router =express.Router()
const db=require('../db')
const utils=require('../utils')

router.get('/all',(request,response)=>{
    const connection=db.openConnection()
    const staetment=`select * from Book`
    connection.query(staetment,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})

router.post('/add',(request,response)=>{
    const connection=db.openConnection()
    const{book_id ,book_title ,publisher_name, author_name}=request.body
    const staetment=`insert into Book(book_id ,book_title ,publisher_name, author_name)
        values(${book_id},'${book_title}','${publisher_name}','${author_name}')`
    connection.query(staetment,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})

router.patch('/update',(request,response)=>{
    const connection=db.openConnection()
    const{book_id,publisher_name}=request.body
    const staetment=`update Book set publisher_name='${publisher_name}' where book_id=${book_id}` 
    connection.query(staetment,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})

router.delete('/delete',(request,response)=>{
    const connection=db.openConnection()
    const{book_id}=request.body
    const staetment=`delete from Book where book_id=${book_id}`
    connection.query(staetment,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })
})
module.exports=router;